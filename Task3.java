/*
 * ВЫВОД НА ЭКРАН ПРЯМОУГОЛЬНОГО ТРЕУГОЛЬНИКА ИЗ ВОСЬМЕРОК
 */

/**
 *
 * @author dia1141b
 */
public class Task3 {

    public static void main(String[] args) {
        /* ПРОХОД ПО ГЛАВНОЙ ДИАГОНАЛЕ И ПОД НЕЙ
        * ЧТО И ЕСТЬ ПРЯМОУГОЛЬНЫЙ ТРЕУГОЛЬНИК 
        */
        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j <= 10; j++) {
                //ВЫВОД НА ЭКРАН ВОСЬМЕРОК 
                if (j <= i) {
                    System.out.print("8");
                }
            }
            //ПЕРЕХОД НА НОВУЮ СТРОКУ
            System.out.println();
        }
    }
}
