/*
 * Вывод четных чисел от 1 до 100
 */

/**
 *
 * @author dia1141b
 */
public class Task1 {

    public static void main(String[] args) {
        int i;
        for (i = 1; i <= 100; i++) {
            //проверка на четность
            if (i % 2 == 0) {
                //вывод
                System.out.print(i + " ");
            }

        }

    }

}
