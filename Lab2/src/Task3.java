/*
 * ВЫВОД НА ЭКРАН ПРЯМОУГОЛЬНОГО ТРЕУГОЛЬНИКА ИЗ 8
 */

/**
 *
 * @author dia1141b
 */
public class Task3 {

    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j <= 10; j++) {
                if (j <= i) {
                    System.out.print("8");
                }
            }
            System.out.println();
        }
    }
}
