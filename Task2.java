/*
 * Рисуем прямоугольник Ввести с клавиатуры два числа m и n.
 *Используя цикл for вывести на экран прямоугольник размером m на n из восьмёрок. 
 */

/**
 *
 * @author dia1141b
 */
import java.util.Scanner;

public class Task2 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int m = 0;
        int n = 0;
        System.out.println("Введите число m");
//проверка исключений
        try {
            m = sc.nextInt();
        } catch (Exception ex) {
            System.out.println("Ошибка ввода " + ex);
        }

        System.out.println("Введите число n");
//проверка исключений
        try {
            n = sc.nextInt();
        } catch (Exception ex) {
            System.out.println("Ошибка ввода " + ex);
        }
        for (int i = 1; i <= m; i++) {
            System.out.println();
            for (int j = 1; j <= n; j++) {
                System.out.print("8");
            }

        }
    }

}
